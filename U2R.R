library(kableExtra)
library(dplyr)
library(VIM)
data_set <- "dataset-stroke-data.csv"
data <- read.csv(data_set, sep=",")
data
colnames(data)
a <- dim(data)
texto <- paste("La dimension del dataset es", a[1], "observaciones y", a[2], "variables")
print(texto)
str(data)
summary(data)
names <- colnames(data)
idQualitative <- which(names=="gender" | names=="hypertension"  |
  names=="heart_disease" | names=="ever_married" |
  names=="work_type" | names=="Residence_type" |
  names=="smoking_status" | names=="stroke")
kable(
sapply(data[idQualitative],class),caption="Vars cualitativas, tipos interpretados") %>%
  kable_styling(latex_options = "HOLD_position")
data <- mutate_at(data, vars(gender, hypertension, heart_disease, ever_married,
                             work_type, Residence_type, smoking_status,
                             stroke),list(factor))
idQuantitative <- which(names=="id" | names=="age" 
                        | names=="avg_glucose_level" |names=="bmi")
kable(
  sapply(data[idQuantitative],class),caption="Vars cuantitativas, tipos interpretados") %>%
  kable_styling(latex_options = "HOLD_position")
data$bmi <- as.numeric(data$bmi)
data
summary(data)

diagrama_bigotes <- function(columna, nombre){
  par(mfrow=c(1, 2))
  boxplot(columna,
          ylab=nombre,
          main=paste(nombre,  "vertical"))
  boxplot(x=columna,
          xlab=nombre,
          main=paste(nombre, "horizontal"),
          horizontal=TRUE)
}
col_name <- colnames(data[idQuantitative])
for (i in col_name){
  diagrama_bigotes(data[, i],  i)
}
col_name <- colnames(data[idQuantitative])
for (i in col_name){
  print(paste("Columna", i))
  outliers <- boxplot.stats(data[, i])$out
  print(outliers)
  if(!is.null(outliers)){
    data[, i] <- ifelse(data[, i] %in%
                          outliers,
                        # TRUE: NA, FALSE: Mismo resultado
                        NA, data[, i])
  }
  diagrama_bigotes(data[, i],  i)
}
trata_na_knn <- function(df, variables, k){
  for (var in variables){
    print(paste(var, "_imp"))
  df <- kNN(df, variable=var, k=k)
  df <- df[ , !(names(df) %in% paste0(var, "_imp"))]}
  return(df)
}
t <- sapply(data, function(x) sum(is.na(x)))
sapply(data, function(x) sum(is.na(x)))
filtro <- (t > 0)
variables_na <- names(t[filtro])
variables_na
data <- trata_na_knn(data, variables_na, k=3)
sapply(data, function(x) sum(is.na(x)))
write.csv(data, file ="dataset2_clean.csv")

